<?php
namespace Justin\FormsBundle\Forms\Types;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class testType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        	->add('field1', 'text', array(
        		'required' => false
        		))
        	->add('number', 'integer')
        	->add('submit', 'submit');
    }

    public function getName()
    {
        return 'test';
    }
}
?>