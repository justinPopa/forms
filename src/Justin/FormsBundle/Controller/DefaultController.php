<?php

namespace Justin\FormsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Justin\FormsBundle\Forms\Types\testType;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new testType());

        $form->handleRequest($request);

        if($form->isValid()){
        	exit('form is valid');
        }

        return $this->render('JustinFormsBundle:forms:form1.html.twig', array(
        		'myForm' => $form->createView())
        	);
    }
}
